import piexif
from pathlib import Path
from GPSPhoto import gpsphoto

def get_geodata(src):
    """
    show datetime, latitude, longitude and altitude of exif photo

    INPUT: str
        - src: path of exif photo
    OUTPUT: tuple
        - datetime, latitude, longitude, altitude
    """
    date, time = (piexif.load(src)['0th'][306]).decode('ascii').split()
    date = date.replace(":", "-")
    dt = f"{date} {time}"
    data = gpsphoto.getGPSData(src)

    return dt, data['Latitude'], data['Longitude'], data['Altitude']

def get_bulk_geodata(src_dir, ftype="*.*"):
    """
    get geodata from a stored photos in src_dir sorted by file names

    INPUT: str
        - src_dir: directory path of exif photos
        - ftype: file type to list, default: *.*
    OUTPUT: dict
        - {'datetime':[], 'latitude':[], 'longitude':[], 'altitude':[]}
    """
    data = {'datetime':[], 'latitude':[], 'longitude':[], 'altitude':[]}
    place = Path(src_dir)
    
    photos = sorted(list(place.glob(ftype)))
    for photo in photos:
        geodata = get_geodata(str(photo))
        data['datetime'].append(geodata[0])
        data['latitude'].append(geodata[1])
        data['longitude'].append(geodata[2])
        data['altitude'].append(geodata[3])

    return data

def main():
    #print(get_geodata(src = "/home/emiliano/data/dev/proyectos_git/LiDAR/out/fotos_drone/DJI_0278.JPG"))
    print(get_bulk_geodata(src_dir="/home/emiliano/data/dev/proyectos_git/LiDAR/out/fotos_drone"))